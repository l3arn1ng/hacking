# Encodage-UU 
- https://www.root-me.org/fr/Challenges/Cryptanalyse/Encodage-UU --24/12/2021-- Très Facile
- Solutions :
    * Solution(s) externes : 10
    * Numéro de la solution : https://www.root-me.org/fr/Challenges/Cryptanalyse/Encodage-UU?action_solution=voir#ancre_solution
    - Propre solution (s) : 0
    - Numéro de la solution : -
    
Ne connaissant pas au préalable le type de chiffrement utilisé, j’ai commencé par chercher et très rapidement j’ai trouvé grâce à “begin” et “end” respectivement au début et à la fin. J’ai alors tranquillement utilisé le site dcode.fr pour déchiffrer. Ainsi j'obtiens dans la chaîne de caractère déchiffrée, **“PASS=ULTRASIMPLE”**. J’ai donc soumis et validé le challenge. 

# Analyse de log - Attaque web (forensic)
- https://www.root-me.org/en/Challenges/Forensic/Logs-analysis-web-attack --9/01/2022-- Medium
- Solutions :
    * Solution(s) externes : 8
    * Numéro de la solution : https://www.root-me.org/en/Challenges/Forensic/Logs-analysis-web-attack?action_solution=voir#ancre_solution
    - Propre solution (s) : 1
    - Numéro de la solution : https://gitlab.com/l3arn1ng/hacking/-/blob/main/Challs/rootme-Forensic01.md

Après avoir vu le log, j’ai constaté qu’il a des caractères codés sur 64 bits. J’ai utilisé l’outil en ligne https://md5decrypt.net/ pour obtenir les caractères originels. Il s’agit de code SQL, certainement de l’**injection SQL**. L’injection a été faite dans le but de récupérer un mot de passe. Pour ce faire, l’auteur à récupérer le binaire du code ascii de chaque caractère du mot de passe. Les caractères sont tous codés sur 7 bits. Il récupère les 6 premiers bits 2 à 2 et le 7ème seul. Il utilise la fonction sleep(n) de sorte que, dans le cas des 6 premiers bits, **n=0 -> 00**, **n=2 -> 01**, **n=4 -> 10**, **n=6 -> 11**. Quant aux 7èmes bits, **n=2 -> 0**, **n=4 -> 1** et ainsi avec **n=0, il n’y a pas de 7 ème bit**. 

J’ai donc relevé les bits de chaque caractère et utilisé le même outil que précédemment pour trouver le mot de passe.
1100111=g, 111001=9, 1010101=U, 1010111=W, 1000100=D, 111000=8, 1000101=E, 1011010=Z, 1100111=g, 1000010=B, 1101000=h, 1000010=B, 1110000=p, 1100011=c, 110100=4, 1101110=n, 1010100=T, 1010011=S, 1000001=A, 1010011=S
D’où le mot de passe : g9UWD8EZgBhBpc4nTSAS

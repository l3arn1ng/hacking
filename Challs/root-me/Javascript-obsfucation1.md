## Javascript-bsfucation1
- https://www.root-me.org/fr/Challenges/Web-Client/Javascript-Obfuscation-1 --23/12/2021-- Très Facile
- Solutions :
    * Solution(s) externes : 10
    * Numéro de la solution : https://www.root-me.org/fr/Challenges/Web-Client/Javascript-Obfuscation-1?action_solution=voir#ancre_solution
    - Propre solution (s) : 0
    - Numéro de la solution : --
    
Ici j’ai inspecté le code source de la page. J’y ai trouvé une chaine de caractère pass composée de caractères hexadécimaux et de ‘%’. J’ai ensuite retrouvé la chaîne normale avec scriptasylum.com. Ce qui donne ‘cpasbiendurpassword’. Ainsi j’ai pu validé le challenge.

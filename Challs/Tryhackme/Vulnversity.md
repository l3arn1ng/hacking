# Vulnversity
- https://tryhackme.com/room/vulnversity --25/12/2021-- Facile
- Solutions :
    * Solution(s) externes : 0
    * Numéro de la solution : -
    - Propre solution (s) : 1
    - Numéro de la solution : https://gitlab.com/l3arn1ng/hacking/-/blob/main/Challs/Tryhackme/Vulnversity.md

J’ai configuré le navigateur pour qu'il utilise le proxy de burpsuite. En utilisant intruder, j’ai effectué une attaque sniper avec comme payload le fichier avec les phpext.txt, ce qui lançait des requêtes avec les différentes extensions du fichier. J’ai eu les mêmes résultats sur chacune des requêtes. J’ai alors uploadé un fichier en phtml et c’est passé. Pour l’escalade de privilèges, ils ont indiqué qu’on peut utiliser systemctl alors j’ai cherché et utilisé ceci https://medium.com/@klockw3rk/privilege-escalation-leveraging-misconfigured-systemctl-permissions-bc62b0b28d49. Et c’est ainsi j’ai retrouvé le flag.

# Docs

Veuillez toujours mettre à jour ce fichier lorsque vous ajoutez un fichier dans ce dossier, pour s'assurer que sa disparition n'est pas du à une suppression accidentel par le push de quelqu'un.**

Si un fichier a été supprimé intentionnellement, veuillez ne pas supprimer sa ligne dans ce fichier mais simplement le marquer comme supprimé et ajouter la raison de la suppression **

**Modèle d'écriture dans ce fichier**

- Répertoire_Du_Doc/Nom_Du_Doc ( Langue ) ( Si_Supprimé )

    - url -- date
    - Categorie(s) : catégorie

- ( Raison(s)_Si_Supprimé )



**Ex** :

Hacking, sécurité et tests d'intrusion avec Metasploit ( FR ) ( DELETED )


https://www.pearson.ch/download/media/9782744066931_SP_1_2.pdf -- 15:05:50
Categorie : Payload

Document possédant beaucoup de mensonges
